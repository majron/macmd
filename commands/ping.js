module.exports = {
  name: "ping",
  execute: (client, message, args, env) => {
    message.channel.send("Pong!");
  },
  desc: "Ping",
  catagory: "test",
  types: [
    "text"
  ],
  alias: [
    "pong"
  ]
};