const fs = require("fs");
const CommandHandler = require("../utils/CommandHandler");
const Handler = new CommandHandler();

fs.readdirSync(__dirname +  "/../commands").forEach((file) => {
  Handler.insertCommand(require(`../commands/${file.split(".")[0]}`));
});

module.exports = {
  name: "message",
  execute: async (client, message) => {
    let Args = message.content.split(" ");
    let CommandArgs = Args.slice(1);

    let CommandPrefix = Args[0].substr(0, process.env.PREFIX.length);
    let Name = Args[0].substr(process.env.PREFIX.length, Args[0].length);
    let Command = Handler.getCommand(Name);
    let Commands = Handler.getCommands();
    function insertGroup(group) {
      Command.insertUserGroup(group);
      Commands.insertUserGroup(group);
    };

    if (message.author.id == process.env.BOT_DEVELOPER) {
      insertGroup("DEVELOPER");
    };

    let Member = message.guild && message.guild.member(message.author) || false;


    if (Command.fetch(message.channel.type) && CommandPrefix == config.prefix) {
      Command.fetch(message.channel.type).execute(client, message, CommandArgs, {
        Handler: Handler,
        Commands: Commands.fetch(message.channel.type),
        prefix: process.env.PREFIX
      });
    };
  }
};