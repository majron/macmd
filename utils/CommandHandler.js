class Handler {
  constructor() {
    this.commands = [];
  };

  getCommands() {
    let userGroups = [];
    return {
      fetch: (channelType) => {
        let availableCommands = [];
        this.commands.forEach((Command) => {
          if (Command.groups && !Command.groups.find(commandGroup => userGroups.includes(commandGroup))) return;
          if (!Command.types.includes(channelType)) return;
          availableCommands.push(Command);
        })
        return availableCommands;
      },
      insertUserGroup: (name) => {
        userGroups.push(name);
      }
    };
  };

  insertCommand(command) {
    this.commands.push(command);
  };

  getCommand(name) {
    let Commands = this.getCommands();
    return {
      fetch: (type) => {
        return Commands.fetch(type).find(command => command.alias.includes(name) || command.name == name || false);
      },
      insertUserGroup: (group) => {
        Commands.insertUserGroup(group);
      }
    };
  };
};

module.exports = Handler;