require("dotenv").config();
const { Client } = require("discord.js");
let client = new Client();
const fs = require("fs");
fs.readdirSync("./events").forEach((file) => { 
  let Event = require(`./events/${file.split(".")[0]}`);
  client.on(Event.name, (...args) => {
    Event.execute(client, ...args);
  });
});



client.recentDeletedMsgs = {};
client.login(process.env.TOKEN);