module.exports = {
  name: "test",
  execute: (client, message, args, env) => {
    message.channel.send("Works!");
  },
  desc: "Says test",
  groups: [
    "DEVELOPER"
  ],
  catagory: "test",
  types: [
    "text"
  ],
  alias: [
    "aliashere"
  ]
};